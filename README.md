---
title: Notes on the process of extracting and preparing these records
author: James P. Ascher
date: 8 June
...

The website `estc.bl.uk` allows you to search strings restricted to
particular time periods, so I found all the records containing certain
strings, saved the MARC-style output, reformatted it to work with
recutils, and then coded the occurrence of apparent licensing
authorities and apparent booksellers.  The code I used is here:

```{elisp}
;; scripts for batch record processing
(defun jpsa/insert-authority-prompt ()
  "In rec-mode, insert a field and prompt for contents."
  (interactive)
  (rec-cmd-append-field)
  (text-scale-set -3)
  (let ((auth-type (read-char "[R]oyal, R.[S]oc., [O]x., [C]am., S[T]ati., [E]din., [D]ublin, [X]norm, lc?")))
    (cond ((equal auth-type ?R) (insert "Authority: Royal"))
          ((equal auth-type ?r) (insert "Authority: Royal?"))
          ((equal auth-type ?O) (insert "Authority: Oxford"))
          ((equal auth-type ?o) (insert "Authority: Oxford?"))
          ((equal auth-type ?C) (insert "Authority: Cambridge"))
          ((equal auth-type ?c) (insert "Authority: Cambridge?"))
          ((equal auth-type ?S) (insert "Authority: Royal Society"))
          ((equal auth-type ?s) (insert "Authority: Royal Society?"))
          ((equal auth-type ?X) (insert "Authority: normal"))
          ((equal auth-type ?x) (insert "Authority: unknown?"))  ; use this for weird things to return to
          ((equal auth-type ?E) (insert "Authority: Edinburgh"))
          ((equal auth-type ?e) (insert "Authority: Edinburgh?"))
          ((equal auth-type ?D) (insert "Authority: Dublin"))
          ((equal auth-type ?d) (insert "Authority: Dublin?"))
          ((equal auth-type ?T) (insert "Authority: Stationers"))
          ((equal auth-type ?t) (insert "Authority: Stationers?"))
          (t (insert "Authority: ERROR TODO review"))))
   (rec-finish-editing)
)

(defun jpsa/insert-bookseller-prompt ()
  "In rec-mode, insert a field and prompt for contents."
  (interactive)
  (rec-cmd-append-field)
  (text-scale-set -3)
  (let ((type (read-char "[A/a]llestry/& others, [M/m]artyn/& others, Martyn [&] Allestry/[7]& others, Sam. [S/s]mith/& others, [W/w]alford/& others, [H/h]ickman/& others, Smith &[*] Walford/[8]& others, [C/c]hiswell/& others, Tho. [J/j]ames/& others [X/x] other/unknown, [P/p]itt/& others, [D/d]avis/& others, [B/b]arker/& others")))
    (cond ((equal type ?P) (insert "Bookseller: Pitt"))
          ((equal type ?p) (insert "Bookseller: Pitt & others"))
          ((equal type ?A) (insert "Bookseller: Allestry"))
          ((equal type ?a) (insert "Bookseller: Allestry & others"))
          ((equal type ?M) (insert "Bookseller: Martyn"))
          ((equal type ?m) (insert "Bookseller: Martyn & others"))
          ((equal type ?D) (insert "Bookseller: Davis"))  ; Richard Davis is of interest here, not John Davies
          ((equal type ?d) (insert "Bookseller: Davis & others"))
          ((equal type ?&) (insert "Bookseller: Martyn & Allestry"))
          ((equal type ?7) (insert "Bookseller: Martyn & Allestry & others"))
          ((equal type ?B) (insert "Bookseller: Barker"))
          ((equal type ?b) (insert "Bookseller: Barker and others"))
          ((equal type ?X) (insert "Bookseller: other"))
          ((equal type ?x) (insert "Bookseller: unknown?"))
          ((equal type ?S) (insert "Bookseller: Sam. Smith"))
          ((equal type ?s) (insert "Bookseller: Sam. Smith & others"))
          ((equal type ?W) (insert "Bookseller: Walford"))
          ((equal type ?w) (insert "Bookseller: Walford & others"))
          ((equal type ?H) (insert "Bookseller: Hickman"))
          ((equal type ?h) (insert "Bookseller: Hickman & others"))
          ((equal type ?*) (insert "Bookseller: Sam. Smith & Walford"))
          ((equal type ?8) (insert "Bookseller: Sam. Smith & Walford & others"))
          ((equal type ?C) (insert "Bookseller: Chiswell"))
          ((equal type ?c) (insert "Bookseller: Chiswell & others"))
          ((equal type ?J) (insert "Bookseller: Tho. James"))
          ((equal type ?j) (insert "Bookseller: Tho. James & others"))
          (t (insert "Bookseller: ERROR TODO review"))))
   (rec-finish-editing)
)

(defun jpsa/insert-field-repeat ()
  "In rec-mode, call 'jpsa/insert-field-propmt', go to next record, repeat"
  (interactive)
  (text-scale-set -3)
  (read-char "Look for authority and publisher")
  (jpsa/insert-authority-prompt)
  (jpsa/insert-bookseller-prompt)
  (rec-cmd-goto-next-rec)
  (jpsa/insert-field-repeat)
)

;; query regexs to clean up data
(defun jpsa/ESTC-rec-insert-date-field ()
  "In a recfile that has MARC data, extract the date from the 008 field and insert a year field."
  (interactive)
  (query-replace-regexp "\\(Tag_008: *.\\{7\\}\\(.\\{4\\}\\).*\\)" "\\1\nYear: \\2")
)

;; regexs to prep ESTC MARC for me
(defun jpsa/ESTC-MARC-to-rec ()
  "Converst the ESTC MARC text file to the recfile I use."
  (interactive)
  ; first convert from DOS line endings to Unix: recutils barfs on dos
  (set-buffer-file-coding-system 'utf-8-unix 't)
  ; these gobble the extra whitespace and rename to fit recfile standards
  (query-replace-regexp "^ \\{5\\}Record number : +\\([0-9]+\\)$" "Record: \\1") 
  (beginning-of-buffer)
  (query-replace-regexp "^ \\{5\\}\\([0-9]\\{3\\}\\)\\(.\\)\\(.\\) \\{16\\}\\(.*\\)$" "Tag_\\1: \\2\\3\\4")
  (beginning-of-buffer)
  (query-replace-regexp "^ \\{5\\}SYS \\{18\\}\\(.*\\)$" "Tag_SYS: \\1")
  ; ESTC seems to use 26 whitespaces to indicate a continuation of the previous line
  ; we do it three times to try to find it all
  (beginning-of-buffer)
  (query-replace-regexp "^\\(.*\\)\n \\{26\\}\\(.*\\)$" "\\1 \\2")
  (beginning-of-buffer)
  (query-replace-regexp "^\\(.*\\)\n \\{26\\}\\(.*\\)$" "\\1 \\2")
  (beginning-of-buffer)
  (query-replace-regexp "^\\(.*\\)\n \\{26\\}\\(.*\\)$" "\\1 \\2")
)
```

The files in the director here have some self documentation, i.e. the
`%doc:` string explains the search query used and what has been
successfully tagged.

The biggest issue here was one of scale and time.  ESTC does not give
extracts of more than one thousand records, which means I cannot—for
example—make a broad query that’s likely to include everything that
might have been licensed by the Royal Society or printed by one of its
printers.  That produces around thirty-two thousand records.  That,
that is too big is fine enough, since it took a few hours to go
through the 754 records represented in the extract containing
everything with “Martyn”, “Martin”, or “Allestry” from 1662 to 1695
and everything with “Royal Society” from 1679 to 1695 to cover the
dates in which Martyn and Allestry were no longer printers.  This, of
course, misses some Latin printing during that time period and some
printing done by Richard Davis in Oxford for the Society.  A
continuation of this project would code the licenses for each of the
printers over this time period, then 1695 to 1720 (say), and combine
the records.

The `recutils` program allows the system code to be the key for a
record and reports duplicates.  It, however, does not make resolving
these duplicates quick.  This would need to be scripted for the larger
project.

## Next version

1. Make sure you have all the Royal Society’s printers listed
   - review your evidence from the records
   - review your evidence from the Council Minutes
   - review the secondary literature
2. Extract each of those printers over, say 1660 to 1695, then 1695 to
   1730, the numbers might need to be tweaked to get around the one
   thousand record limit.
3. Code the records, one at a time, over a period of days.
4. Review the coded records and look for patterns.

## Hypothesis

I think that there will be distinct linguistic patterns in the
imprint, “to the Royal Society” etc. which vary in meaning over time.
By the early 19th century, I think that the prepositional “to” has
lost its sense of authorizing the work and merely indicates a certain
quality of printer, but prior to 1680 it seems to me the “to” was
clearly indicating the authority for printing and licensing.

Additionally, I’m particularly interested in the very irregular items.
Some printers produce only one item, others seem not to have been
noted in some sources.  What does this imply?
