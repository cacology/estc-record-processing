#!/bin/bash

# generates deduped type Book from whatever matches
# it expects two sets of records, properly setup as
#
# %rec: Book
# %type: Tag_001 rec Book2
#
# %rec: Book2
# %key: Tag_001
#
# It returns the a "deduped" file with the records of Book type
# deleted and a "diff" file with the differences between the fields of
# those records

recsel -t Book -j Tag_001 $1 > dups.$1
recsel -p Tag_SYS,Tag_001_Tag_001,Authority,Tag_001_Authority,Bookseller,Tag_001_Bookseller dups.$1 > dups.diff.$1
recsel -P Tag_001_Tag_001 dups.$1 > dups.Tag_001.$1.txt

cp $1 deduped.$1

for line in $(cat dups.Tag_001.$1.txt)
do
 recdel -t Book -e "Tag_001 = $line" deduped.$1
 #recsel -t Book -e "Tag_001 = $line" deduped.$1
 #echo "-e Tag_001 = '$line'"
done

rm dups.Tag_001.$1.txt dups.$1
