#!/bin/bash

echo "Counting records '$1 && Year = 1662-95'"

for i in {1662..1695}
do
            recsel -c -e "$1 && Year = $i" MARS.rec
done
